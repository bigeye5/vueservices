package com.example.basics.vueservices.rest;

import com.example.basics.vueservices.model.Person;
import com.example.basics.vueservices.repositories.PersonRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PersonController {
    private PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/person")
    public Iterable<Person> findAll() {
        return personRepository.findAll();
    }
}
