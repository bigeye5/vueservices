package com.example.basics.vueservices.bootstrap;

import com.example.basics.vueservices.model.Person;
import com.example.basics.vueservices.repositories.PersonRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private PersonRepository personRepository;

    public DevBootstrap(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private void initData() {
        Person max = new Person("Max", 27, "Red");
        Person anna = new Person("Anna", 20, "Blue");

        personRepository.save(max);
        personRepository.save(anna);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }
}
