package com.example.basics.vueservices.repositories;

import com.example.basics.vueservices.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
