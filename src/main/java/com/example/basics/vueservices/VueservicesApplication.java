package com.example.basics.vueservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(VueservicesApplication.class, args);
	}

}
